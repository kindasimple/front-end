qpl
=====

## Challange*

Create a listing of qualified parts that supports search and paging.

## Quickstart*

*"Production"*

Serve build artifacts using an nginx http server and proxy

```bash 
docker-compose up -d
open http://localhost:3000
```

*Development* 

The environment requires global installations of npm. Api requests are proxied to avoid failing CORS requests which require Authorization. 

```bash
npm install
npm start
open http://localhost:3000
```

Optionally, (optionally) storybook

```bash
npm i -g @storybook/cli #may not be necessary
npm run storybook
```


## Solution

I chose react rather than angular because of my familiarity with it. Hopefully the  evaluator has a passing familiarity. Using a familiar framework allows for me to demonstrate my practices and preferences. If I had written an angular project my project structure would have been a mismatch of angular-specific conventions found online, and I thought it would take more time and be confusing.

The project is intended to be simple, and there are some development considerations to support this. For example, both css modules and sass compilation requires some simple webpack configuration which requires ejecting the application to accomplish. To avoid this, some command scripts are added to `package.json` to handle this preprocessing outside of webpack. 

The data model followed is the popular flux library implementing unidirectional data flow and immutable types.

Static typing is achieved through Facebook's Flow framework, because it compliments react. Declaring and specifying types looks similar to Typescript.

Class and method interface documentation through comments is, for the most part, omitted to save time. VS Code can infer the interface in a pinch

## Next Steps

The UI layout is tuned for the Chrome browser. The layout appears fine in Firefox but must be modified for Safari and is untested on IE or Edge.

The unit test configuration will need to be customized to support our test cases. Additional tests could be added where needed.

The navigation bar on the bottom left is an approximation. An icon set isn't available in the project to support these buttons, so it was easier to approximate them. Without metadata, it isn't possible to have a go-to-end button ">|", because the page index isn't known. Even if it could be specified (with offset=-1), the forward offset, i.e. page number, woulnd't be known without a separate api call or some metadata from the request. 


## Project structure

    /build              /* build artifacts */
    /src                /* application sourcecode */
    - /actions              /* Server/View Actions */
    |
    - /containers           /* Smart components */
    |   |- App.js               /* Dumb application container */
    |   |- ViewQplList.js       /* Primary application container */
    |
    - /components           /* Pure presentation componenents */
    |   |- [*].js           /* View Components */
    |   |- [*].scss         /* Component Styles */
    |       
    - /dispatcher           /* Vanilla Flux dispatcher */
    |
    - /stores
    |   |- PartsStore.js    /* flux store to map actions, requests, 
    |                            and updates */
    - /stories              /* storybook component views */
    |
    - / styles              /* shared sass files */
    |
    - /utils
    |   |- WebUtils.js      /* api requests */
    |   |- Utils.js         /* functions that manage api requests */
    |
    |- index.js             /* React bootstrapping */

*Tests*

Test configuration needs some bootstrapping. Mounting containers is failing. So are calls to the fetch api.

Debugging tests can be achieved in VSCode by adding the following configuration to launch.json.

```json
"version": "0.2.0",
    "configurations": [
      {
        "name": "Debug CRA Tests",
        "type": "node",
        "request": "launch",
        "runtimeExecutable": "${workspaceRoot}/front-end/node_modules/.bin/react-scripts",
        "runtimeArgs": [
          "--inspect", "--debug-brk",
          "test", "-i"
        ],
        "args": [
          "--runInBand",
          "--no-cache",
          "--env=jsdom"
        ],
        "cwd": "${workspaceRoot}/front-end",
        "protocol": "inspector",
        "console": "integratedTerminal",
        "internalConsoleOptions": "neverOpen"
      }
    ]
```