// @flow

import type { QPL } from '../types'

/**
 * Match the search query against the appropriate fields in the object
 * @param {QPL} item The Qualified Part to search through 
 * @param {string} searchQuery The text to search for
 * @returns True if match, False otherwise
 */
export function matchPart (item: QPL, searchQuery: string) {
    // NOTE: Matching wont work on date fields
    // NOTE: Not an efficient search, but our set is small (20 items)
    for(let field in item) {
        if(item[field] && 
            ['string', 'number'].indexOf(typeof(item[field])) !== -1 &&  
            ("" + item[field]).match(new RegExp(searchQuery, "ig"))) {
            return true
        }
    }
    return false
}

export function getApiKey(): string {
    return process.env.REACT_APP_API_KEY || "8A8B747C-8320-4237-AC3B-D82F8A101B6D"
}