// @flow
import type { QPL, Response } from '../types'

export function makeUri(origin: string, route: string, options: Object): string {
    return origin + route + getQueryString(options)
}

export function getQueryString(options: any): string {
    let keys = Object.keys(options);
    return keys.length > 0
        ? keys.reduce((acc, key) => {  
                return acc + ((acc.length > 2 ) ? "&" : "") + `${key}=` + options[key]
            }, "?")
        : ""
}

/**
 * Guard against invalid status codes
 * @param {Object} response Response payload
 * @returns {Object} Response payload
 */
export function checkStatus(response: Response): Response {
    if (response.status >= 200 && response.status < 300) {
        return response
    } else {
        var error = new Error(response.statusText)
        throw error
    }
}

/**
 * Get JSON object from the response object
 * @param {Object} response 
 * @returns {Object} JSON serialized response
 */
export function parseJSON(response: Response): Response | Array<QPL> { 
    return response.json() 
}