import { getQueryString } from './apiUtils'

describe('request', () => {
    it('gets a query string', () => {
        return expect(getQueryString({
            p1: "p1",
            p2: "p2"
        }))
        .toEqual("?p1=p1&p2=p2")
    });
    it('get a 1 parameter querystring', () => {
        return expect(getQueryString({ p1: "p1"}))
                .toEqual("?p1=p1")
    });
    it('gets an empty object', () => {
        return expect(getQueryString({}))
                .toEqual("")
    });
})
