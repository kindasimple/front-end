export type Action = {
    type: string,
    payload: ?Object,
    error: ?boolean
}

export type QPL = {
    id: number,
    partNumber: string,
    revision: string,
    partName: string,
    toolDieSetNumber: number,
    isQualified: false,
    openPo: true,
    jurisdiction: null,
    classification: null,
    supplierName: string,
    supplierCodes: string,
    ctq: true,
    lastUpdatedBy: string,
    lastUpdatedDateUtc: Date,
    qplExpirationDate: Date,
    uppapStatus: string,
    uppapExpiresUtc: Date,
    uppapUpdatedBy: string,
    uppapUpdatedDateUtc: Date,
}

export type Response = {
    json: () => Any,
    status: number,
    statusText: string,
}