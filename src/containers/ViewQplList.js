
import * as React from 'react';
import { Container } from 'flux/utils';
import PartsStore from '../stores/PartsStore'
import { fetchParts, applySearch, setPageIndex, setNumItems } from '../actions/ViewActions'
import './ViewQplList.css';

import type { QPL } from '../types'

import QplList from '../components/QplList'
import Heading from '../components/Heading'
import Search from '../components/Search'

import {ENTER_KEYCODE} from '../utils/constants'

type State = {
    isFetching: boolean,
    searchQuery?: string,
    items: Array<QPL>,
}

class ViewQplList extends React.Component<void, void, State> {

    static getStores() {
        return [PartsStore];
    }

    componentDidMount() {
        this.removeChangeListener = PartsStore.addListener(this._onUpdate.bind(this));
    }

    componentWillMount() {
        fetchParts(Object.assign({}, this.props))
    }

    componentWillUnmount() {
        this.removeChangeListener(this._onUpdate.bind(this))
    }

    _onUpdate() {
		this.setState(PartsStore.getState());
    }

    static calculateState(prevState: State) {
        return Object.assign({}, prevState, { items: PartsStore.getParts().asMutable() } )
    }

    handleSearch = (searchQuery: string) => {
        applySearch({ searchQuery })
    }

    handleNumItemsChange = (numItems) => {
        setNumItems({ numItems })
    }

    handleNavigate = (type) => {
        let pageIndex = this.state.pageIndex
        switch(type) {
            case "back":
                pageIndex--;
                break;
            case "forward":
                pageIndex++;
                break;
            default:
            case "start": 
                pageIndex = 0;
                break;
            case "end":
                pageIndex = 0 // TODO: go to last
                break;
        }

        if(pageIndex < 0) { pageIndex = 0 }
        setPageIndex({ pageIndex })
        
    }

    handleSearchChange = (event) => {
        this.setState({ searchQuery: event.target.value })
    }

    handleKeyPress = (event) => {
        if(event.keyCode === ENTER_KEYCODE) {
            this.handleSearch(this.state.searchQuery)
        }
    }

    render() {
        return <div className="view-qpl-list">
                <Heading>
                    <div className="qpl-search">
                        <Search searchQuery={this.state.searchQuery} onSearch={this.handleSearch} onChange={this.handleSearchChange} onKeyDown={this.handleKeyPress}/>
                    </div>
                </Heading>
                <QplList {...this.props} {...this.state} onNavigate={this.handleNavigate} onNumItemsChange={this.handleNumItemsChange} />
            </div>
    }
}

export default Container.create(ViewQplList, {withProps: true});
