// @flow
import * as React from 'react';
import './App.css';

import QplList  from './ViewQplList'

class App extends React.Component<{ numPage: number, numItems: number }> {
    static defaultProps = {
        numItems: 20,
        numPage: 0
    }

    render() {
        return (
            <div className="App">
                <QplList pageIndex={0} numItems={20} />
            </div>
        );
    }
}

export default App;
