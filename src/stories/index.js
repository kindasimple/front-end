import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Button, Welcome } from '@storybook/react/demo';

import Search from '../components/Search'
import Status from '../components/Status'
import Navigation from '../components/Navigation'
import QplListItem from '../components/QplListItem'
import QplListHeading from '../components/QplListHeading'
import QplList from '../components/QplList'
import { page1, page2 } from '../mocks'

import '../reset.css'
import '../index.css'

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

storiesOf('Button', module)
    .add('with text', () => <Button onClick={action('clicked')}>Hello Button</Button>)
    .add('with some emoji', () => <Button onClick={action('clicked')}>😀 😎 👍 💯</Button>);

storiesOf('Search', module)
    .add('Empty', () => <Search />)
    .add('with text', () => <Search searchQuery="Some Query" />)

storiesOf('Status', module)
    .add('default', () => <Status />)
    .add('page one size 25', () => <Status numItems={20} pageIndex={0} />)

function onNavigate(input) {
    alert(input)
}
storiesOf('Navigation', module)
    .add('default', () => <Navigation />)
    .add('zero and twenty', () => <Navigation pageIndex={1} numItems={20} />)

storiesOf('QplListHeading', module)
    .add('default', () => <QplListHeading  />)

storiesOf('QplListItem', module)
    .add('default', () => <QplListItem  />)
    .add('page1', () => <QplListItem {...page1[0]} />)

    storiesOf('QplList', module)
    .add('default', () => <QplList  />)
    .add('page1', () => <QplList {...{ items: page1 }} />)