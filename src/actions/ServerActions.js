 /*
 * @flow
 */
import { dispatch } from "../dispatcher"

import type { Action, QPL } from '../types'


export function partsFetched (payload: ?QPL[], error: ?boolean = false) : Action {
    dispatch({ 
        type: "PARTS_FETCHED", payload, error
    })
}