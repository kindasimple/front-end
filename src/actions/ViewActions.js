import {dispatch} from '../dispatcher'

export function fetchParts (payload: Object) {
    dispatch({ type: "PARTS_FETCH", payload })
}

export function applySearch (payload: Object) {
    dispatch({ type: "SEARCH_APPLY", payload })
}

export function setPageIndex(payload: Object) {
    dispatch({ type: "PAGE_INDEX_SET", payload })
}

export function setNumItems(payload: Object) {
    dispatch({ type: "ITEMS_SET", payload })
}