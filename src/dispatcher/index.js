/*
 * @flow
 */

import type {Action} from '../types';
import { Dispatcher } from 'flux';

const instance: Dispatcher<Action> = new Dispatcher();

export default instance;
export const dispatch = instance.dispatch.bind(instance);
