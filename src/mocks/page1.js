export const page1 = [
    {
        "id": 44,
        "partNumber": "01086",
        "revision": "DE",
        "partName": "Russell67",
        "toolDieSetNumber": "30934",
        "isQualified": false,
        "openPo": false,
        "jurisdiction": null,
        "classification": null,
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": true,
        "lastUpdatedBy": "Jorge.Goodman",
        "lastUpdatedDateUtc": "2017-09-23T07:37:41.9",
        "qplExpirationDate": "2017-10-31T00:00:00",
        "uppapStatus": "Full Approval",
        "uppapExpiresUtc": "2017-10-18T16:00:04.71",
        "uppapUpdatedBy": "Jorge.Goodman",
        "uppapUpdatedDateUtc": "2017-10-24T20:02:58.067"
    },
    {
        "id": 30,
        "partNumber": "01251",
        "revision": "LL",
        "partName": "Marla",
        "toolDieSetNumber": "58473",
        "isQualified": false,
        "openPo": true,
        "jurisdiction": null,
        "classification": null,
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": true,
        "lastUpdatedBy": "Dianne",
        "lastUpdatedDateUtc": "2017-09-07T16:00:04.71",
        "qplExpirationDate": "2017-10-31T00:00:00",
        "uppapStatus": "Full Approval",
        "uppapExpiresUtc": "2017-10-18T16:00:04.71",
        "uppapUpdatedBy": "Dianne",
        "uppapUpdatedDateUtc": "2017-10-24T20:04:13.007"
    },
    {
        "id": 29,
        "partNumber": "01835",
        "revision": "VE",
        "partName": "Kristi6",
        "toolDieSetNumber": "83582",
        "isQualified": true,
        "openPo": false,
        "jurisdiction": null,
        "classification": "Important",
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": false,
        "lastUpdatedBy": "Dwight.Richards",
        "lastUpdatedDateUtc": "2017-09-10T22:26:38.69",
        "qplExpirationDate": null,
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 55,
        "partNumber": "03707",
        "revision": "NE",
        "partName": "Melvin5",
        "toolDieSetNumber": "87239",
        "isQualified": false,
        "openPo": true,
        "jurisdiction": null,
        "classification": "Important",
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": true,
        "lastUpdatedBy": "Jorge.Goodman",
        "lastUpdatedDateUtc": "2017-06-28T12:07:21.01",
        "qplExpirationDate": null,
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 91,
        "partNumber": "04291",
        "revision": "QE",
        "partName": "Marlene5",
        "toolDieSetNumber": "48940",
        "isQualified": true,
        "openPo": false,
        "jurisdiction": null,
        "classification": "Important",
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": true,
        "lastUpdatedBy": "Jim.Thompson",
        "lastUpdatedDateUtc": "2017-09-14T11:07:07.92",
        "qplExpirationDate": "2018-01-10T17:01:24.75",
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 51,
        "partNumber": "09095",
        "revision": "EV",
        "partName": "Stephanie45",
        "toolDieSetNumber": "68461",
        "isQualified": false,
        "openPo": false,
        "jurisdiction": null,
        "classification": null,
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": false,
        "lastUpdatedBy": "Dwight.Richards",
        "lastUpdatedDateUtc": "2017-09-20T12:50:48.52",
        "qplExpirationDate": "2018-10-23T07:05:11.81",
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 92,
        "partNumber": "09856",
        "revision": "LU",
        "partName": "Candice975",
        "toolDieSetNumber": "37882",
        "isQualified": false,
        "openPo": true,
        "jurisdiction": "ITAR",
        "classification": "Important",
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": false,
        "lastUpdatedBy": "Dianne",
        "lastUpdatedDateUtc": "2017-08-01T22:07:43.65",
        "qplExpirationDate": null,
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 23,
        "partNumber": "11109",
        "revision": "MF",
        "partName": "Michele",
        "toolDieSetNumber": "68961",
        "isQualified": true,
        "openPo": false,
        "jurisdiction": "ITAR",
        "classification": null,
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": true,
        "lastUpdatedBy": "Dwight.Richards",
        "lastUpdatedDateUtc": "2017-08-19T22:19:06.43",
        "qplExpirationDate": null,
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 57,
        "partNumber": "12708",
        "revision": "CR",
        "partName": "Elias84",
        "toolDieSetNumber": "09478",
        "isQualified": true,
        "openPo": false,
        "jurisdiction": null,
        "classification": "Important",
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": true,
        "lastUpdatedBy": "Dianne",
        "lastUpdatedDateUtc": "2017-09-27T10:25:24.2",
        "qplExpirationDate": null,
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 63,
        "partNumber": "12945",
        "revision": "WY",
        "partName": "Jody0",
        "toolDieSetNumber": "50082",
        "isQualified": true,
        "openPo": true,
        "jurisdiction": "ITAR",
        "classification": null,
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": false,
        "lastUpdatedBy": "Dwight.Richards",
        "lastUpdatedDateUtc": "2017-09-28T02:20:41.89",
        "qplExpirationDate": null,
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 85,
        "partNumber": "13398",
        "revision": "X",
        "partName": "Nina68",
        "toolDieSetNumber": "13255",
        "isQualified": false,
        "openPo": true,
        "jurisdiction": "ITAR",
        "classification": null,
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": true,
        "lastUpdatedBy": "Jorge.Goodman",
        "lastUpdatedDateUtc": "2017-09-18T03:09:16.2",
        "qplExpirationDate": "2018-10-12T17:33:09.05",
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 86,
        "partNumber": "14009",
        "revision": "XK",
        "partName": "Alice26",
        "toolDieSetNumber": "61782",
        "isQualified": true,
        "openPo": false,
        "jurisdiction": null,
        "classification": null,
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": true,
        "lastUpdatedBy": "Jim.Thompson",
        "lastUpdatedDateUtc": "2017-06-13T04:25:50.63",
        "qplExpirationDate": "2018-02-09T16:27:59.43",
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 39,
        "partNumber": "15584",
        "revision": "TX",
        "partName": "Carl6",
        "toolDieSetNumber": "82631",
        "isQualified": true,
        "openPo": true,
        "jurisdiction": "ITAR",
        "classification": "Important",
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": false,
        "lastUpdatedBy": "Dwight.Richards",
        "lastUpdatedDateUtc": "2017-07-24T12:58:49.09",
        "qplExpirationDate": "2018-04-03T14:13:47.35",
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 88,
        "partNumber": "15749",
        "revision": "TS",
        "partName": "Julio0",
        "toolDieSetNumber": "81622",
        "isQualified": true,
        "openPo": false,
        "jurisdiction": "ITAR",
        "classification": "Important",
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": true,
        "lastUpdatedBy": "Dwight.Richards",
        "lastUpdatedDateUtc": "2017-09-24T10:35:21.34",
        "qplExpirationDate": null,
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 60,
        "partNumber": "15826",
        "revision": "BD",
        "partName": "Clayton062",
        "toolDieSetNumber": "76366",
        "isQualified": false,
        "openPo": true,
        "jurisdiction": "ITAR",
        "classification": "Important",
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": true,
        "lastUpdatedBy": "Jim.Thompson",
        "lastUpdatedDateUtc": "2017-09-03T23:11:48.99",
        "qplExpirationDate": null,
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 70,
        "partNumber": "16506",
        "revision": "ZC",
        "partName": "Vicki2",
        "toolDieSetNumber": "81248",
        "isQualified": true,
        "openPo": false,
        "jurisdiction": null,
        "classification": null,
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": false,
        "lastUpdatedBy": "Dwight.Richards",
        "lastUpdatedDateUtc": "2017-09-03T06:09:46.48",
        "qplExpirationDate": "2018-08-08T07:23:38.49",
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 42,
        "partNumber": "16673",
        "revision": "OL",
        "partName": "Ronald87",
        "toolDieSetNumber": "82868",
        "isQualified": false,
        "openPo": false,
        "jurisdiction": "ITAR",
        "classification": null,
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": false,
        "lastUpdatedBy": "Dianne",
        "lastUpdatedDateUtc": "2017-08-31T08:06:57.72",
        "qplExpirationDate": "2018-03-16T03:23:12.55",
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 40,
        "partNumber": "16816",
        "revision": "WS",
        "partName": "Emma4",
        "toolDieSetNumber": "52599",
        "isQualified": false,
        "openPo": false,
        "jurisdiction": null,
        "classification": "Important",
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": true,
        "lastUpdatedBy": "Jim.Thompson",
        "lastUpdatedDateUtc": "2017-06-14T22:47:43.28",
        "qplExpirationDate": null,
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 27,
        "partNumber": "19193",
        "revision": "OK",
        "partName": null,
        "toolDieSetNumber": "28163",
        "isQualified": true,
        "openPo": false,
        "jurisdiction": "ITAR",
        "classification": "Important",
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": true,
        "lastUpdatedBy": "Jorge.Goodman",
        "lastUpdatedDateUtc": "2017-07-25T19:10:09.75",
        "qplExpirationDate": null,
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    },
    {
        "id": 50,
        "partNumber": "19609",
        "revision": "EB",
        "partName": "Eric72",
        "toolDieSetNumber": "04079",
        "isQualified": true,
        "openPo": false,
        "jurisdiction": null,
        "classification": "Important",
        "supplierName": "Acme Corporation",
        "supplierCodes": "",
        "ctq": true,
        "lastUpdatedBy": "Jorge.Goodman",
        "lastUpdatedDateUtc": "2017-09-09T11:31:31.77",
        "qplExpirationDate": "2018-11-12T09:54:47.72",
        "uppapStatus": null,
        "uppapExpiresUtc": null,
        "uppapUpdatedBy": null,
        "uppapUpdatedDateUtc": null
    }
]