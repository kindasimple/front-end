// @flow
import {
    checkStatus, 
    makeUri, 
    parseJSON
} from './utils/apiUtils'
import { getApiKey } from './utils'
import { partsFetched } from './actions/ServerActions'

import type { QPL } from './types'

const API_ORIGIN = /*process.env.REACT_APP_API_ORIGIN || */'http://localhost:3000'
const route = {
    qpl: "/qpl"
}

const requestInit = {
    method: 'GET',
    headers: {
        Authorization: 'key ' + getApiKey(),
    }
}

/**
 * Fetch parts list and send message to store when complete
 * @param {number} offset Zero-based page offset
 * @param {number} pageSize Number of parts to retrieve
 * @returns {Promise}
 */
export function fetchParts(offset: number, pageSize: number) : Promise<QPL> {
    const url = makeUri(API_ORIGIN, route.qpl, {offset, pageSize})
    return new Promise(function(res, rej) {
        fetch(url, requestInit)
        .then(checkStatus)
        .then(parseJSON)
        .then(function (response: QPL[]) {
            if(response) {
                partsFetched(response, false)
                res(response)
            } else {
                partsFetched(null, true)
                rej({ message: "Missing Data"})
            }
        })
        .catch(function (err) {
            rej(err)
        })
    })
}