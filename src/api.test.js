import { fetchParts } from './api'
import { page1, page2} from './mocks'

describe('request', () => {
    const expected = [
        page1,
        page2
    ]
    it('fetches first page', () => {
        const itemCount = 20
        expect.assertions(2);
        return expect(fetchParts(0, 20))
                .resolves
                .toEqual(expect.arrayContaining(expected[0]))
                .toHaveLength(itemCount);
    });

    it('fetches second page', () => {
        const itemCount = 20
        expect.assertions(2);
        return expect(fetchParts(1, itemCount))
                .resolves
                .toEqual(expect.arrayContaining(expected[1]))
                .toHaveLength(itemCount);
    });

    it('changes item count', () => {
        const itemCount = 5
        expect.assertions(1);
        return expect(fetchParts(0, itemCount))
                .resolves
                .toHaveLength(itemCount);
    });
})
