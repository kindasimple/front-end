/*
 * @flow
 */

import type {Action} from '../types'
import Dispatcher from '../dispatcher'
import { ReduceStore } from 'flux/utils'

import { fetchParts } from '../api'
import { matchPart } from '../utils'
import Immutable from 'seamless-immutable'

import type {QPL} from '../types'


/**
 * Flux data store containing 
 */
class PartsStores extends ReduceStore<Object> {

    getInitialState (): Object {
        return new Immutable({ items: [] })
    }

    getParts (): Array<QPL> {
        const state = this.getState();
        return state.items.slice(0, state.numItems) //reduce to page size
                            .filter(item => matchPart(item, state.searchQuery))
    }

    reduce(state: Object, action: Action): Object {
        switch(action.type) {
            case 'ITEMS_SET': {
                const { numItems } = action.payload
                if(numItems > state.numItems) {
                    fetchParts(state.pageIndex, numItems)
                }
                return state.set("numItems", numItems)
            }

            case 'PAGE_INDEX_SET':
            case 'PARTS_FETCH': {
                const {pageIndex, numItems} = Object.assign({}, state, action.payload)
                debugger;
                fetchParts(pageIndex, numItems)
                return state.merge({
                    pageIndex,
                    numItems,
                    isFetching: true
                })
            }

            case 'SEARCH_APPLY': {
                return state.set("searchQuery", action.payload.searchQuery)
            }

            case 'PARTS_FETCHED': {
                if(action.error) {
                    //TODO: Handle error
                    return state.set("isFetching", false)
                } else {
                    return state.merge({
                        items: action.payload,
                        isFetching: false,
                    })
                }
            }

            default:
                return state
        }
    }
}

export default new PartsStores(Dispatcher)