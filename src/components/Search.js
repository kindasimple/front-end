// @flow
import * as React from 'react'

import './Search.css'
import ButtonIcon from './ButtonIcon'
import icon from "../images/search.svg" 

type Props = {
    searchQuery: string,
    onSearch: (searchQuery: string) => void,
    onChange: (e: any) => void,
    onKeyDown: (e: any) => void,
};

export default function Search({ searchQuery, onSearch, onChange, onKeyDown }: Props) {
    return <div className="search" style={{ whiteSpace: 'nowrap' }}>
            <input value={searchQuery} onChange={onChange} onKeyDown={onKeyDown} placeholder="Search..." />
            <ButtonIcon styles={{padding: "8px 10px 10px 10px"}} onClick={() => { onSearch(searchQuery) }} src={icon} alt="search"/>
        </div>
}

Search.defaultProps = {
    searchQuery: "",
    onSearch: null
}