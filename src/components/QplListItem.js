
// @flow
import * as React from 'react'
import DateField from './DateField'
import NullableField from './NullableField'
import QplListRow from './QplListRow'
import './QplListItem.css'

import type { QPL } from '../types';

export default function QplListItem(props: QPL) 
{
    return <QplListRow>
            <div className="list-item">
            <div className="part-number">{props.partNumber}</div>
            <div>{props.revision}</div>
            <div>{props.partName}</div>
            <div>{props.toolDieSetNumber}</div>
            <div>{props.isQualified}</div>
            <div>{props.openPo}</div>
            <div>{props.jurisdiction}</div>
            <div>{props.classification}</div>
            <div>{props.supplierName}</div>
            <div>{props.supplierCodes}</div>
            <div>{props.ctq}</div>
            <div><NullableField value={props.lastupdatedBy} /></div>
            <div><DateField value={props.lastUpdatedDateUtc} /></div>
        </div>
    </QplListRow>
}