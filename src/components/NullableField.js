// @flow
import * as React from 'react'

type Props = {
    children?: React.Node
}

export default function NullableField(props: Props): React.Node
{
    return props.children || <div>--</div>
}

