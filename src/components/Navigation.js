// @flow
import * as React from 'react'

import './Navigation.css'
import Dropdown from './Dropdown'
import Button from './Button'

type Props = {
    styles?: Object,
    numItems: number,
    onNavigate: (string) => void,
    onNumItemsChange: (number) => void,
    pageIndex: number,
};

export default function Navigation({ 
    styles,
    numItems, 
    onNavigate,
    onNumItemsChange,
    pageIndex, 
}: Props) {
    return <div className="navigation-container">
                <Button value="|&#x276c;" onClick={() => onNavigate("start")} />
                <Button value="&#x276c;" onClick={() => onNavigate("back")} />
                {`${pageIndex}`}
                <Button value="❭" onClick={() => onNavigate("forward")} />
                <Button value="❭|" onClick={() => onNavigate("end")} />
                <Dropdown activeItem={numItems} maxItems={20} 
                        onChange={(e) => { debugger;onNumItemsChange(parseInt(e.target.value, 10)) }} /> 
                <div style={{ fontSize: "0.7rem", display: 'inline-block'}}>items per page</div>
            </div>
}

Navigation.defaultProps = {
    numItems: 0,
    pageIndex: 0,
    onNavigate: () => {},
    onNumItemsChange: () => {},
}
