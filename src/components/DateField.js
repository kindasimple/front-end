// @flow
import * as React from 'react'
var ReactIntl = require('react-intl');

var FormattedDate = ReactIntl.FormattedDate;
var IntlProvider = ReactIntl.IntlProvider;

export default function DateField({ value }: { value: Date }): React.Node
{
    return (value) 
                ? <IntlProvider locale="en">
                    <FormattedDate value={value} day="numeric" month="numeric" year="2-digit" />
                </IntlProvider>
                : <div>--</div>
}

