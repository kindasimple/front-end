
// @flow
import * as React from 'react'

import './QplListRow.css'

type Props = {
    children?: React.Node,
  };

export default function QplListRow({children} : Props) 
{
    return <div className="list-row">
        { children }
    </div>
}