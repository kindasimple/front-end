// @flow
import * as React from 'react'

import './Heading.css'

type Props = {
    children?: React.ChildrenArray<any>
}

export default function Heading({ children }: Props) 
{
    return <div className="list-heading">
        { children }
    </div>
}