// @flow
import * as React from 'react'

import './Status.css'

type Props = {
    numItems: number,
    pageIndex: number,
};

export default function Status({ 
    numItems, 
    pageIndex, 
}: Props) {
    const startIndex = pageIndex * numItems + 1
    const endIndex = (pageIndex + 1) * numItems
    return <div className="status">Showing items {`${startIndex} - ${endIndex}`}</div>
}

Status.defaultProps = {
    numItems: 0,
    pageIndex: 0,
}