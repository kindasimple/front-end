// @flow
import * as React from 'react'

import './Button.css'

type Props = {
    styles?: Object,
    value: string,
    onClick: () => void,
    children?: React.ChildrenArray<any>,
}

export default function Button ({ styles, value, onClick, children }: Props) 
{
    return <button style={styles} onClick={onClick}>
            { children || value }
        </button>
}

Button.defaultProps = {
    value: "",
    onClick: void 0
}