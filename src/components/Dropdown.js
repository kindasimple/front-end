// @flow
import * as React from 'react'

import './Dropdown.css'

export default class Dropdown extends React.Component<{maxItems: number, activeItem: number, onChange: (e: any) => void}>
{
    static defaultProps = {
        maxItems: 20,
        activeItem: 0,
        onChange: () => {},
    }

    renderOptions (maxItems: number): Array<React.Node> {
        let options = []
        for(let i=1;i<=maxItems;i++) {
            options.push(<option key={"dropdown_" + i } value={i}>{i}</option>)
        }
        return options
    }

    render () {
        return <div className="dropdown">
                    <select value={this.props.activeItem} onChange={this.props.onChange}>
                        { this.renderOptions(this.props.maxItems) }
                    </select>
                    <div className="dropdown-after"/>
                </div>
    }

}