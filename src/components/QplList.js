
// @flow
import * as React from 'react'
import QplListHeading from './QplListHeading'
import QplListItem from './QplListItem'
import Status from './Status'
import Navigation from './Navigation'
import Heading from './Heading'
import QplListRow from './QplListRow'

import './QplList.css'

import type { QPL } from '../types';

type Props = { 
    items: Array<QPL>, 
    numItems: number, 
    pageIndex: number,
    onNavigate: (value: string) => void,
    onNumItemsChange: (value: number) => void,
}

export default function QplList({ items, numItems, pageIndex, onNavigate, onNumItemsChange } : Props) 
{
    return <div className="list">
        <QplListHeading />
        { items.map((item: QPL, idx: number) => <QplListItem key={`qpl_${idx}`} {...item} />) }
        <Heading styles={{textAlign: 'left'}}>
            <QplListRow>
                <Navigation {...{numItems, pageIndex, onNavigate, onNumItemsChange}} />
                <div className="list-status">
                    <Status />
                </div>
            </QplListRow>
        </Heading>
    </div>
}

QplList.defaultProps = {
    items: []
}