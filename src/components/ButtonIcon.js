// @flow
import * as React from 'react'
import Button from './Button'
import './ButtonIcon.css'

type Props = {
    styles?: Object,
    src?: string,
    alt?: string,
    onClick: (e: any) => void,
    children?: React.ChildrenArray<any>,
}

export default function ButtonIcon ({ styles, src, alt, onClick, children }: Props) 
{
    return <Button onClick={onClick} styles={styles}>
            { children || <img className="button-image" src={src} alt={alt} />}
            </Button>
}

Button.defaultProps = {
    value: "",
    onClick: void 0,
    alt: ""
}