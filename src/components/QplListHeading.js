
// @flow
import * as React from 'react'
import Heading from './Heading'
import './QplListHeading.css'

import type { QPL } from '../types';

export default function QplListItem(props: QPL) 
{
    return <Heading>
        <div className="list-item list-labels">
            <div>Part Number</div>
            <div>Part Revision</div>
            <div>Part Name</div>
            <div>Tool / Die Set Number</div>
            <div>QPL</div>
            <div>Open PO</div>
            <div>Part Jurisdiction</div>
            <div>Part Classification</div>
            <div>Supplier Company Name</div>
            <div>Supplier Company Code</div>
            <div>CTQ</div>
            <div>QPL Last Updated By</div>
            <div>QPL Last Updated Date</div>
        </div>
    </Heading>
}